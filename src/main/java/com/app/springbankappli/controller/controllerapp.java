package com.app.springbankappli.controller;

import org.springframework.stereotype.Controller;
import org.springframework.ui.ModelMap;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.ResponseBody;

@Controller
public class controllerapp {
	/**
	 * Load  index page
	 * @return
	 */
	
	@GetMapping("/")        
	public String Index(ModelMap  modelmap) {
		
		String nomeclient="Hugo";
		
		//modelmap.put("Hugo",nomeclient);
		
		modelmap.addAttribute("name", nomeclient);
		
		return "index";
	}
	
	@GetMapping("view/contact")        
	public String Contact() {
		return "view/contact";
	}
	
	@GetMapping("view/about")        
	public String About() {
		return "view/about";   
	}
	
	@GetMapping("view/news")        
	public String News() {
		return "view/news";   
	}

}
